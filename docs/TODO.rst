spiders
------------------
* esmarket product spider
* esmarket category spider

pipelines
------------------
* SQLite db with SQLAlchemy

database
------------------
* to csv
* to json

docs
------------------
* usage
* installation

other
------------------
* publish on PyPI