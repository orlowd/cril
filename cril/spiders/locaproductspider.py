# -*- coding: utf-8 -*-

import re

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector

from cril.items import ProductItem

class LocaProductSpider(CrawlSpider):
    '''
    Spider looking for products in online shop lokaah.pl.
    '''

    name = 'LocaProductSpider'
    allowed_domains = ['lokaah.pl']
    start_urls = ['http://lokaah.pl/pl/']
    rules = (
        Rule(SgmlLinkExtractor(
            restrict_xpaths=('//li[@id="pagination_next"]')),
            follow=True),
        Rule(SgmlLinkExtractor(
            restrict_xpaths=('//div[@id="categories_block_left"]//a')),
            follow=True),
        Rule(SgmlLinkExtractor(
            restrict_xpaths=('//ul[@id="product_list"]/li/div/h3')),
            follow=True, callback='parse_product')
    )

    def parse_product(self, response):
        sel = Selector(response)
        product = ProductItem()

        product['url'] = response.url
        product['product_id'] = re.search(r'\d+', response.url).group()
        product['name_h1'] = sel.xpath(
            ('//div[@id="primary_block"]/h1/text()')).extract()
        product['short_description'] = sel.xpath(
            ('//div[@id="short_description_content"]/p/text()')).extract()
        product['head_title'] = sel.xpath('//head/title/text()').extract()
        product['head_meta_description'] = sel.xpath(
            ('//meta[@name="description"]/@content')).extract()
        price = sel.xpath('//span[@id="our_price_display"]/text()').extract()
        # remove currency
        product['price'] = price[0].split(' ')[0]
        descriptions = sel.xpath('//div[@id="idTab1"]/p/text()').extract()
        if descriptions:
            product['long_description'] = "\n".join(descriptions)
        categories = sel.xpath(
            ('//div[@class="breadcrumb"]/a[position()>1]/text()')).extract()
        if categories:
            product['category'] = "/".join(categories)
        old_price = sel.xpath('//span[@id="old_price_display"]/text()').extract()
        # remove currency
        if old_price:
            product['old_price'] = old_price[0].split(' ')[0]
        is_available = sel.xpath('//span[@id="availability_value"]/text()').extract()
        #FIXME ugly condition
        if is_available[0] == u'\n\t\t\t\t\t\t\t\t\t':
            is_available = 'true'
        else:
            is_available = 'false'
        product['is_available'] = is_available
        product['on_sale'] = sel.xpath('//span[@class="on_sale"]/text()').extract()

        discount = sel.xpath('//span[@class="discount"]/text()').extract()
        if discount:
            product['discount'] = discount

        return product
