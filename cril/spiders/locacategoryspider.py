# -*- coding: utf-8 -*-

from scrapy.spider import Spider
from scrapy.selector import Selector

from cril.items import CategoryItem

class LocaCategorySpider(Spider):
    '''
    Spider looking for product categories in online shop lokaah.pl.
    '''

    name = 'LocaCategorySpider'
    allowed_domains = ['lokaah.pl/']
    start_urls = ['http://lokaah.pl/pl/']

    def parse(self, response):
        sel = Selector(response)
        categories_sel = sel.xpath('//div[@id="categories_block_left"]//a')
        categories = []
        for category_sel in categories_sel:
            category = CategoryItem()
            #category['name'] = category_sel.xpath('text()').extract()
            category['url'] = category_sel.xpath('@href').extract()
            category['description'] = category_sel.xpath('@title').extract()
            categories.append(category)
        return categories
