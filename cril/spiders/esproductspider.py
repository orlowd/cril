# -*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector

from cril.items import EsProductItem, ItemConst

class EsProductSpider(CrawlSpider):
    '''
    Spider looking for products in online shop esmarket.pl.
    '''

    name = 'EsProductSpider'
    allowed_domains = ['esmarket.pl']
    start_urls = ['http://esmarket.pl/wyszukaj']
    rules = (
        Rule(SgmlLinkExtractor(
            restrict_xpaths=('//span[@class="arrows arrows-next"]')),
            follow=True),
        Rule(SgmlLinkExtractor(
            restrict_xpaths=('//div[@class="produkt-cat"]')),
            follow=True, callback='parse_product')
    )

    def parse_product(self, response):
        sel = Selector(response)
        product = EsProductItem()

        product['url'] = response.url
        product['product_id'] = ItemConst.NOT_APPLICABLE
        product['name_h1'], = sel.xpath('//h1[@id="offer_title"]/text()').extract()
        product['short_description'] = ItemConst.NOT_APPLICABLE
        product['head_title'], = sel.xpath('//head/title/text()').extract()
        product['head_meta_description'], = sel.xpath(
           ('//meta[@name="description"]/@content')).extract()
        product['price'] = sel.xpath(
           ('//div[@class="present_price"]/h3/text()')).extract()[0]
        properities = dict(zip(
            sel.xpath('//td[@class="title"]/h4/text()').extract(),
            sel.xpath('//td[@class="text"]/h4/text()').extract()))
        if properities:
            product['properities'] = properities
        categories = sel.xpath('//div[@id="breadcrumbs"]/ul/li/a/text()').extract()
        if categories:
            product['category'] = "/".join(categories)
        descriptions = sel.xpath('//div[@id="zakladki-opis"]/p/text()').extract()
        if descriptions:
            product['long_description'] = "\n".join(descriptions)
        product['old_price'] = sel.xpath('//div[@class="old_price"]/h4/text()').extract()
        product['is_available'] = sel.xpath('//div[@class="info_dost"]/h5').extract()
        product['on_sale'] = ItemConst.NOT_APPLICABLE
        product['discount'] = sel.xpath('//div[@class="level name"]/div[2]/text()').extract()

        return product
