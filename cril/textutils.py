# -*- coding: utf-8 -*-

unicode_to_ascii_dict = ({
    u'\u0105': 'a',
    u'\u0107': 'c',
    u'\u0119': 'e',
    u'\u0142': 'l',
    u'\u0144': 'n',
    u'\u00F3': 'o',
    u'\u015B': 's',
    u'\u017A': 'z',
    u'\u017C': 'z',
    u'\u015A': 'S',
    u'\u017B': 'Z',
    })

def unicode_to_ascii(text):
    for key in unicode_to_ascii_dict:
        text = text.replace(key, unicode_to_ascii_dict[key])
    return text
