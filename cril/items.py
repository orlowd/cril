# -*- coding: utf-8 -*-

from scrapy.item import Item, Field

class ItemConst(object):
    NOT_APPLICABLE = 'n/a'
    TODO = 'TODO'
    TRUE = 'true'
    FALSE = 'false'


class CategoryItem(Item):
    name = Field()
    url = Field()
    description = Field()


class ProductItem(Item):
    product_id = Field()
    name_h1 = Field()
    short_description = Field()
    long_description = Field()
    url = Field()
    price = Field()
    discount = Field()
    old_price = Field()
    is_available = Field()
    on_sale = Field()
    category = Field()
    head_title = Field()
    head_meta_description = Field()
    properities = Field(default='')


class EsProductItem(ProductItem):
    rodzaj_dekoru = Field(default=ItemConst.NOT_APPLICABLE)
    opakowanie = Field(default=ItemConst.NOT_APPLICABLE)
    czas_schniecia_h = Field(default=ItemConst.NOT_APPLICABLE)
    zastosowanie = Field(default=ItemConst.NOT_APPLICABLE)
    wymiary_mm = Field(default=ItemConst.NOT_APPLICABLE)
    kod_producenta = Field(default=ItemConst.NOT_APPLICABLE)
    ilosc_w_paczce_szt = Field(default=ItemConst.NOT_APPLICABLE)
    wysokosc_mm = Field(default=ItemConst.NOT_APPLICABLE)
    glebokosc_cm = Field(default=ItemConst.NOT_APPLICABLE)
    szerokosc_mm = Field(default=ItemConst.NOT_APPLICABLE)
    klosz_abazur = Field(default=ItemConst.NOT_APPLICABLE)
    model_produktu = Field(default=ItemConst.NOT_APPLICABLE)
    rozmiar = Field(default=ItemConst.NOT_APPLICABLE)
    wymiary_m2_paczka = Field(default=ItemConst.NOT_APPLICABLE)
    szerokosc_ciecia_cm = Field(default=ItemConst.NOT_APPLICABLE)
    abazur_klosz = Field(default=ItemConst.NOT_APPLICABLE)
    mrozoodporna = Field(default=ItemConst.NOT_APPLICABLE)
    wysokosc_cm = Field(default=ItemConst.NOT_APPLICABLE)
    gwarancja = Field(default=ItemConst.NOT_APPLICABLE)
    drenaz = Field(default=ItemConst.NOT_APPLICABLE)
    metody_aplikacji = Field(default=ItemConst.NOT_APPLICABLE)
    zarowka = Field(default=ItemConst.NOT_APPLICABLE)
    struktura = Field(default=ItemConst.NOT_APPLICABLE)
    indukcja = Field(default=ItemConst.NOT_APPLICABLE)
    oznaczenia_specjalne = Field(default=ItemConst.NOT_APPLICABLE)
    stopien_ochrony_ip = Field(default=ItemConst.NOT_APPLICABLE)
    producent = Field(default=ItemConst.NOT_APPLICABLE)
    wbudowany_akumulator = Field(default=ItemConst.NOT_APPLICABLE)
    kolor_abazuru_klosza = Field(default=ItemConst.NOT_APPLICABLE)
    dlugosc_cm = Field(default=ItemConst.NOT_APPLICABLE)
    cena_m2 = Field(default=ItemConst.NOT_APPLICABLE)
    srednica_cm = Field(default=ItemConst.NOT_APPLICABLE)
    substancja_rozcienczajaca = Field(default=ItemConst.NOT_APPLICABLE)
    pojemnosc_l = Field(default=ItemConst.NOT_APPLICABLE)
    wydajnosc_m2l = Field(default=ItemConst.NOT_APPLICABLE)
    dlugosc_mm = Field(default=ItemConst.NOT_APPLICABLE)
    opakowanie_lkg = Field(default=ItemConst.NOT_APPLICABLE)
    srednica_mm = Field(default=ItemConst.NOT_APPLICABLE)
    kolor = Field(default=ItemConst.NOT_APPLICABLE)
    gornadolna_srednica_cm = Field(default=ItemConst.NOT_APPLICABLE)
    pasowanie_wzoru = Field(default=ItemConst.NOT_APPLICABLE)
    seria_produktu = Field(default=ItemConst.NOT_APPLICABLE)
    tworzywo = Field(default=ItemConst.NOT_APPLICABLE)
    pojemnosc = Field(default=ItemConst.NOT_APPLICABLE)
    wymiary_sztuka = Field(default=ItemConst.NOT_APPLICABLE)
    liczba_elementow = Field(default=ItemConst.NOT_APPLICABLE)
    dlugosc_sznura_mm = Field(default=ItemConst.NOT_APPLICABLE)
    sklad_zestawu = Field(default=ItemConst.NOT_APPLICABLE)
    wykonczenie = Field(default=ItemConst.NOT_APPLICABLE)
    rodzaj_produktu = Field(default=ItemConst.NOT_APPLICABLE)
    model = Field(default=ItemConst.NOT_APPLICABLE)
    rodzaj_laczenia = Field(default=ItemConst.NOT_APPLICABLE)
    model_silnika = Field()
    typ_silnika = Field()
