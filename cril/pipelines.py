# -*- coding: utf-8 -*-

from functools import wraps

from scrapy import signals
from scrapy.contrib.exporter import CsvItemExporter

from cril.textutils import unicode_to_ascii

class DummyPipeline(object):

    def process_item(self, item, spider):
        return item


class UseWith(object):

    def __init__(self, spiders_name_list):
        self.names = spiders_name_list

    def __call__(self, pipeline_method):
        names = self.names

        @wraps(pipeline_method)
        def wrapper(self, item, spider):
            if spider.name in names:
                return pipeline_method(self, item, spider)
            else:
                return item
        return wrapper


class CsvExportPipeline(object):

    def __init__(self):
        self.files = {}

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self, spider):
        file = open('%s_products.csv' % spider.name, 'w+b')
        self.files[spider] = file
        self.exporter = CsvItemExporter(file)
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item


class EsItemPipeline(object):

    _process_key_dict = ([
        unicode_to_ascii,
        lambda x: x.lower(),
        lambda x: x.replace(' ', '_'),
        lambda x: x.replace('(', '_'),
        lambda x: ''.join(c for c in x if c not in ')[]/.:'),
        lambda x: x.replace('__', '_'),
        ])

    @UseWith(['EsProductSpider'])
    def process_item(self, item, spider):
        if 'properities' in item:
            for key, value in item['properities'].iteritems():
                key = self._process_key(key)
                value = self._process_value(value)
                item[key] = value
            item['properities'] = ''
        return item

    def _process_key(self, key):
        for func in self._process_key_dict:
            key = func(key)
        return key

    def _process_value(self, value):
        return ''.join(c for c in value if c not in '\n\t')
