# -*- coding: utf-8 -*-

BOT_NAME = 'Cril'

SPIDER_MODULES = ['cril.spiders']
NEWSPIDER_MODULE = 'cril.spiders'

DOWNLOAD_DELAY = 0.05 # 250 ms

USER_AGENT = 'CrilSpider (+gomc-2014-studenci@googlegroups.com)'

ITEM_PIPELINES = {
    'cril.pipelines.EsItemPipeline': 300,
    'cril.pipelines.CsvExportPipeline': 900,
}

LOG_LEVEL = 'INFO'
#LOG_FILE = 'log.txt'
