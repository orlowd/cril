# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='Cril',
    version='0.1',
    description='Scrapy project for GOMC',
    author='Daniel Orłowski',
    author_email='orlowd@gmail.com',
    url='https://bitbucket.org/orlowd/cril/'
    packages=find_packages(),
    entry_points={'cril': ['settings = cril.settings']},
)